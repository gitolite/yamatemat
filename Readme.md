Yet Another Matemat
===================

Mate calculation as a web app.
We are developing a python3 django app.


Installation
------------

On a Debian System install: python3-django sqlite
Install suggested dependcies as well (sqlite).

then run (in the repo root):

./manage.py syncdb

create an admin user for the new django database, then run

./manage.py runserver

and you should already be able to access it at: http://localhost:8000/

Now all that's left is to add drinkers and drinks via the admin panel.
Access it at http://localhost:8000/admin/ (don't miss the last slash)
and add at least one drinker and one drink using the admin user you just created.
Alternatively you can pull out mate.sqlite3 from the 'database' branch, which already
contains some drinkers and drinks.

If you don't know nothing about django yet, we strongly suggest you read the tutorial:
https://docs.djangoproject.com/en/1.7/intro/tutorial01/

We welcome if you send us how you made it run on your system!
