from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'matemat.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #(r'.*$', 'matecnt.views.overview'),
    (r'index$', 'matecnt.views.index'),
    (r'users/.*$', 'matecnt.views.users'),
    (r'user/(?P<code>\w+)$', 'matecnt.views.user'),
    (r'drinks/.*$', 'matecnt.views.drinks'),
    (r'drink/(?P<code>\w+)$', 'matecnt.views.drink'),
    (r'checkout', 'matecnt.views.checkout'),
    (r'charge', 'matecnt.views.charge')
)
