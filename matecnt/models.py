from django.db import models

class CodeTagged(models.Model):

    name = models.CharField(max_length=123)
    code = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return '<%r %s>' % (self.name, self.code)

    class Meta:
        abstract = True


class Drinker(CodeTagged):
    credit = models.IntegerField(default=0)


class Drink(CodeTagged):
    prize = models.IntegerField(default=3)
