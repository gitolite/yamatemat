from django.contrib import admin

from matecnt import models

class DrinkerAdmin(admin.ModelAdmin):

    fields = ['name', 'code', 'credit']
    list_display = ['name', 'code', 'credit']

admin.site.register(models.Drinker, DrinkerAdmin)


class DrinkAdmin(admin.ModelAdmin):

    fields = ['name', 'code', 'prize']
    list_display = ['name', 'code', 'prize']

admin.site.register(models.Drink, DrinkAdmin)

