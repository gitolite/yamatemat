from django.shortcuts import render, redirect, get_object_or_404

from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext, loader

from matecnt.models import Drinker, Drink

# Helpers

def get_users():
    return Drinker.objects.order_by('credit')

def get_user(code):
    return get_object_or_404(Drinker, code=code)

def get_drink(code):
    return get_object_or_404(Drink, code=code)

def get_drinks():
    return Drink.objects.order_by('prize')


# Views

def index(request):
    ctx = {
        'heading': 'Welcome to YAMatemat',
        'drinkers': get_users(),
    }
    return render(request, 'index.html', ctx)


def user(request, code=None):

    if not code:
        if 'user' in request.REQUEST:
            code = request.REQUEST['user']
        else:
            return redirect('/mate/users/')

    user = get_user(code)

    ctx = {
        'heading': 'User %s' % (user.name),
        'drinker': user,
        'drinks': get_drinks,
    }
    return render(request, 'user.html', ctx)


def drink(request, code=None):

    if not code:
        if 'drink' in request.REQUEST:
            code = request.REQUEST['drink']
        else:
            return redirect('/mate/drinks/')

    drink = get_drink(code)


    ctx = {
        'heading': 'Drink %s' % (drink.name),
        'drink': drink,
        'drinkers': get_users(),
    }
    return render(request, 'drink.html', ctx)


def users(request):
    users = get_users()

    ctx = {
        'heading': 'User list',
        'drinkers': users,
    }
    return render(request, 'users.html', ctx)

def drinks(request):
    drinks = get_drinks()

    ctx = {
        'heading': 'Drink list',
        'drinks': drinks,
    }
    return render(request, 'drinks.html', ctx)


# Actions (POSTs)

def checkout(request):

    #if not request.method == 'POST':
        #raise RuntimeError('only POST allowed')

    try:
        user = request.REQUEST['drinker']
        drink = request.REQUEST['drink']
    except KeyError as ex:
        return HttpResponse(status=400)

    count = int(request.REQUEST.get('count', 1))
    drink_data = get_drink(drink)
    total = drink_data.prize * count
    drinker = get_user(user)
    drinker.credit -= total
    drinker.save()

    ctx = {
        'heading': 'Codes scanned: user=%r drink=%r amount=%s' % (user, drink, count),
        'drinker': drinker,
        'drink': drink_data,
        'total': total,
    }
    return render(request, 'checkout.html', ctx)


def charge(request):

    if not request.method == 'POST':
        raise RuntimeError('only POST allowed')

    try:
        user = request.POST['drinker']
        charge = int(request.POST['charge'])
    except KeyError as ex:
        return HttpResponse(status=400)

    drinker = get_user(user)
    drinker.credit += charge
    drinker.save()

    ctx = {
        'heading': 'Charging: user=%r amount=%s' % (user, charge),
        'drinker': drinker,
        'amount': charge,
    }
    return render(request, 'charge.html', ctx)

