from django.conf.urls import patterns, include, url
from django.contrib import admin

from django.views.generic import RedirectView

from matecnt import urls as matecnt_urls

urlpatterns = patterns('',
    url(r'^mate/', include(matecnt_urls)),
    url(r'^admin/', include(admin.site.urls)),
    (r'^.*$', RedirectView.as_view(url='/mate/index')),
)
